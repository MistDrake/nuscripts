# nuscripts

## [url parse git](scripts/url-parse-git.nu)
Parses git urls into a record table.
<pre>
❯ 'git@gitlab.com:MistDrake/nuscripts.git' | url parse git
╭────────┬─────────────────────╮
│ scheme │                     │
│ user   │ git                 │
│ pass   │                     │
│ host   │ gitlab.com          │
│ port   │                     │
│ path   │ MistDrake/nuscripts │
╰────────┴─────────────────────╯
</pre>