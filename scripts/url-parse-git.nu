export def "url parse git" []: string -> record {
    let url = $in 
    let default = {
        scheme: null, 
        user: null, 
        pass:null, 
        host: null, 
        port: null
        path: null,
    }
    let url_record = $url 
        | parse --regex '(?:(?<scheme>https?|ssh)://)?(?:(?<user>.+)@)?(?<host>.+\.\w{1,3})(?::(?<port>\d+))?[/:](?<path>.*)' 
        | str replace --regex ".git$" "" path
        | into record 

    if ($url_record | is-empty) {
        error make {
            msg: "Unsupported input",
            label: {
                text: "Incomplete or incorrect URL. Expected a fully qualified git URL, e.g., https://github.com/nushell/nushell.git",
    
            }
        }
    }
    default $default | merge $url_record
}
